import sys
import time
from sfa256 import hasher

ITERATIONS = 10
METHODS = {'-e': 'hash', '-c': 'check'}


def _usage():
    usage = """Usage: python3 main.py [-e password | -c password_hash]
        methods:
        -e password,                  encrypt password
        -c password password_hash,    check password hash"""
    return usage


def parse_method(method):
    if method not in METHODS.keys():
        print("Invalid option: %s" % method)
        print(_usage())
        exit()
    return METHODS[method]


def main(argv):
    if len(argv) < 2:
        print(_usage())
        exit(0)

    method = parse_method(argv[0])

    if method == 'hash':
        iterations = ITERATIONS
        salt = int(time.time())
        asd = hasher.hash(argv[1], salt, iterations)
        hash_string = '%s:%s:%s' % (asd, salt, iterations)
        print(hash_string)
    elif method == 'check':
        if len(argv) < 3:
            print(_usage())
            exit(0)
        pwd = argv[1]
        hsh = argv[2]
        print(hasher.check(pwd, hsh))
    else:
        print(_usage())


if __name__ == '__main__':
    main(sys.argv[1:])
