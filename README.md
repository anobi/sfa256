# Stupid Fucking Algorithm 256
Password hashing algorithm based on shitty JPEG compression.

## Demo (lol)
```
(.env) ➜  Stupidhack2019 git:(master) ✗ python3 main.py
Usage: python3 main.py [-e password | -c password_hash]
        methods:
        -e password,                  encrypt password
        -c password password_hash,    check password hash

// encrypt the password    
(.env) ➜  Stupidhack2019 git:(master) ✗ python3 main.py -e hunter2
w5YBbWR7FcKNPGvDq8KAwobCocKkWx3DiH19EnfCpHBKwqQWVxAQwpkOw4PCqsOEDCJLw6NEw40dDiAsEsOtw605aTLCjMOGw4fCj0PCv01Cwo/DnXDCqFcl:1558177024:10

// check a valid password against the hash
(.env) ➜  Stupidhack2019 git:(master) ✗ python3 main.py -c hunter2 w5YBbWR7FcKNPGvDq8KAwobCocKkWx3DiH19EnfCpHBKwqQWVxAQwpkOw4PCqsOEDCJLw6NEw40dDiAsEsOtw605aTLCjMOGw4fCj0PCv01Cwo/DnXDCqFcl:1558177024:10
True

// check invalid password
(.env) ➜  Stupidhack2019 git:(master) ✗ python3 main.py -c nothunter2 w5YBbWR7FcKNPGvDq8KAwobCocKkWx3DiH19EnfCpHBKwqQWVxAQwpkOw4PCqsOEDCJLw6NEw40dDiAsEsOtw605aTLCjMOGw4fCj0PCv01Cwo/DnXDCqFcl:1558177024:10
False
```

## Hashing process
1) create an image of the password
* <img src="screenshots/Screenshot 2019-05-18 at 14.41.55.png" width="300" />
2) add some noise to get more data to compress
* <img src="screenshots/Screenshot 2019-05-18 at 14.42.10.png" width="300" />
3) compress the image n times on a quality setting of 1
* <img src="screenshots/Screenshot 2019-05-18 at 14.42.18.png" width="300" />
4) read bytes, do some unnecessary XOR magic to get the length down to more manageable levels, append salt and compression iterations
5) ???
6) Profit!
* <img src="screenshots/Screenshot 2019-05-18 at 14.42.54.png" />