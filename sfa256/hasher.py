import numpy as np
import base64
from sfa256.imager import generate_image, fuck_shit_up


def get_seed(salt):
    return int(salt)


def hash(password, salt, iterations):
    seed = get_seed(salt)
    img = generate_image(password, seed)
    img = fuck_shit_up(img, seed, iterations)

    img_bytes = bytearray(np.array(img).tobytes())
    while len(img_bytes) % 256 != 0:
        img_bytes.append(0)

    out = bytearray()
    nbytes = 64
    bx = int(len(img_bytes) / nbytes)
    for x in range(0, nbytes):
        bts = img_bytes[x:x+bx]
        bto = bts[0]
        for i in range(1, len(bts)):
            bto = bto ^ bts[i]
        out.append(bto)

    bytestring = ''.join([chr(b) for b in out])
    bytehash = base64.b64encode(bytes(bytestring, 'utf-8')).decode()
    return bytehash


def check(password, hashstring):
    pw, salt, iterations = hashstring.split(':')
    iterations = int(iterations)
    hashed = hash(password, salt, iterations)
    return pw == hashed
