import numpy as np
from PIL import Image


def get_noise(seed, size):
    np.random.seed(seed)
    gauss = np.random.normal(1.0, 1.0, (size[1], size[0]))
    return Image.fromarray(gauss, 'L')
