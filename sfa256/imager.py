import numpy as np
from io import BytesIO
from PIL import Image, ImageDraw, ImageFont, ImageEnhance

from sfa256.utils import get_noise

FONT = ImageFont.truetype('/Library/Fonts/Comic Sans MS.ttf', 30)


def generate_image(password, seed):
    (size, offset) = FONT.font.getsize(password)

    bg = get_noise(seed, size).convert('RGB')
    contrast = ImageEnhance.Contrast(bg)
    bg = contrast.enhance(seed)

    text = Image.new('RGB', size, (255, 255, 255, 255))
    d = ImageDraw.Draw(text)
    d.text((0, -offset[1]), password, font=FONT, fill=(0, 0, 0, 255))

    out = Image.blend(text, bg, 0.5)
    contrast = ImageEnhance.Contrast(out)
    out = contrast.enhance(seed / 2.0)

    return out.convert("L")


def compress_image(image, quality):
    image = image.convert("RGB")
    with BytesIO() as buffer:
        image.save(buffer, format='JPEG', quality=quality)
        return Image.open(buffer).convert("L")


def add_noise(image, seed):
    np.random.seed(seed)
    img_a = np.array(image)
    gauss = np.random.normal(1.0, 1.0, img_a.shape)
    gauss = gauss.reshape(img_a.shape[0], img_a.shape[1])
    noise = Image.fromarray(img_a / gauss, 'L')
    return Image.blend(image, noise, 0.5)


def shitify(image, iterations):
    shit = image
    for i in range(0, iterations):
        shit = compress_image(shit, 1)
    return shit


def fuck_shit_up(image, seed, iterations):
    out = image
    for i in range(0, iterations):
        noise = add_noise(image, seed)
        noised = Image.blend(image, noise, 0.5)
        out = shitify(noised, i)

    return out
